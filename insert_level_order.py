# Recursive Python program for level order traversal of Binary Tree 
  
# A node structure 
class Node: 
  
    # A utility function to create a new node 
    def __init__(self, key): 
        self.data = key  
        self.left = None
        self.right = None
  
  
# Function to  print level order traversal of tree 
def printLevelOrder(root): 
    h = height(root) 
    for i in range(1, h+1): 
        new_data = printGivenLevel(root, i, data=[]) 
        #print new_data
        print "---"
        
        
  
  
# Print nodes at a given level 
def printGivenLevel(root , level,data): 
    if root is None: 
        return data
    if level == 1: 
        #data.append(root.data)
        print root.data
        return data
    elif level > 1 : 
        printGivenLevel(root.left , level-1, data) 
        printGivenLevel(root.right , level-1, data) 
    return data
  
  
""" Compute the height of a tree--the number of nodes 
    along the longest path from the root node down to 
    the farthest leaf node 
"""
def height(node): 
    if node is None: 
        return 0 
    else : 
        # Compute the height of each subtree  
        lheight = height(node.left) 
        rheight = height(node.right) 
  
        #Use the larger one 
        if lheight > rheight : 
            return lheight+1
        else: 
            return rheight+1
  
# Driver program to test above function 
root = Node(1) 
root.left = Node(2) 
root.right = Node(3) 

root.right.right = Node(7) 
root.right.left = Node(6) 
root.left.right = Node(5)
root.left.left = Node(4)

root.left.left.left = Node(8)
root.left.left.right = Node(9)
root.left.right.left = Node(10)
root.left.right.right = Node(11)
root.right.left.left = Node(12)
root.right.left.right = Node(13)
root.right.right.left = Node(14)
root.right.right.right = Node(15)
############################################
##########################################
##########################################

def zig_zag_level_order(root):
    if root is None:
        return 
    current = []
    next = [] 
    current.append(root)
    ltr = True
    while (len(current) > 0 ):

        temp = current.pop(-1)
        print "current node ", temp.data
        if ltr : 
            if temp.left : next.append(temp.left)
            if temp.right : next.append(temp.right)
        else : 
            if temp.right : next.append(temp.right)
            if temp.left : next.append(temp.left)
        
        if len(current) == 0 : 
            # print "~~~~~~~~~~~~~~~~~"
            # for i in next : 
            #     print i.data
            # print "~~~~~~~~~~~~~~~~~"
            ltr = not ltr
            current , next = next , current
                

def level_order(root):
    if root is None:
        return 
    
    current = []
    next = [] 
    level = 0
    current.append(root)
    
    while (len(current) > 0 ):
        temp = current.pop(0)
        print "current node ", temp.data
        if temp.left : next.append(temp.left)
        if temp.right : next.append(temp.right)

        if len(current) == 0 : 
            # print "~~~~~~~~~~~~~~~~~"
            # for i in next : 
            #     print i.data
            # print "~~~~~~~~~~~~~~~~~"
            current , next = next , current
             

def insert_level_order(arr,root,i,n):
    if i < n : 
        temp = Node(arr[i])
        root = temp
        root.left = insert_level_order(arr, root.left, 2*i+1, n )
        root.right = insert_level_order(arr, root.right, 2*i+2, n )
    return root 
    
    
print("Level order traversal of binary tree is -") 
arr = [1,2,3,4,5,6,7,8,9,10]
n=len(arr)
root = None 
i = 0 
root = insert_level_order(arr,root,i,n)
# level_order(root)
print "################################"
print "########## ZIG ZAG #############"
print "################################"
level_order(root)
#zig_zag_level_order(root)

  