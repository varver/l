
# missing numbers
    

def get_sum(arr, sum_till_this_value=0):
    sum = 0 
    for i in range(0,len(arr)) : 
        val = arr[i]
        if sum_till_this_value == 0 : 
            sum += val 
        elif val <= sum_till_this_value : 
            sum += val 
    #print "sum is ", sum
    return sum
    

def sum_till(n): 
    k = (n*(n+1))/2
    #print "sum till is ", k
    return k
    

def find_missing_numbers(arr,n):
    missing_values_sum = sum_till(n) - get_sum(arr)
    #print missing_values_sum
    avg = int(missing_values_sum / 2)
    #print avg 
    first_missing = sum_till(avg) - get_sum(arr,avg)
    second_missing = missing_values_sum - first_missing
    print first_missing
    print second_missing
    

if __name__ == "__main__" : 
    find_missing_numbers([1,2,5,6,7],7)