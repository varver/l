class Node:
    def __init__(self,val):
        self.data = val
        self.left = None
        self.right = None
        


def inorder(root):
    if root is not None : 
        inorder(root.left)
        print root.data
        inorder(root.right)



n1 = Node(1)
n1.left = Node(2)
n1.right = Node(3)
n1.right.right = Node(4)
n1.right.left = Node(5)
n1.left.right = Node(6)
n1.left.left = Node(7)


inorder(n1)
