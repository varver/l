package main

import (
    "golang.org/x/net/publicsuffix"
    //"io/ioutil"
    "log"
    "net/http"
    "net/http/cookiejar"
    "net/url"
    "fmt"
    "time"
    //"sync"
    //"os"
    "github.com/PuerkitoBio/goquery"
    "strings"
    "runtime"
    "flag"
   // "gopkg.in/robfig/cron.v2"
    //"bytes"
)


var TestMode bool
var User string
var SlotTime string
var Today bool

func SetFlags(){
    flag.BoolVar(&TestMode, "test", false, "set true to enable test mode")
    flag.BoolVar(&Today, "today", false, "set true to book for today")
    flag.StringVar(&User, "user", "isha", "use isha|akhil|mehul")
    flag.StringVar(&SlotTime, "time", "20:00", "time like 22:30")
    flag.Parse()
    if TestMode {
        fmt.Println(TestMode)
        fmt.Println(Today)
        fmt.Println(User)
        fmt.Println(SlotTime)
    }
}

type Slot struct {
    id string
    start_hrs  string
    start_mins string
    end_hrs string
    end_mins string
}


func Make_slots(s *map[string]Slot, value,text string){
    // extract values first 
    text = strings.Replace(text, " ", "",-1)
    timestamp := strings.Split(text,"-")
    start := strings.Split(timestamp[0],":")
    end := strings.Split(timestamp[1],":")
    end_mins := strings.Replace(end[1], "00", "0",-1)
    start_mins := strings.Replace(start[1], "00", "0",-1)
    key := timestamp[0]

    var slot = Slot{id: value, start_hrs: start[0], end_hrs: end[0], start_mins: start_mins, end_mins: end_mins }
    (*s)[key]= slot
}


type Setup struct {
    start_time string
    date string
    username  string
    password string
    facility_id string
    block string
    ru_id string
    slot Slot
    ru_num string
}


func (s *Setup) Load(params ...Slot){
    loc, _ := time.LoadLocation("Asia/Kolkata")
    now := time.Now().In(loc)
    var date_tomorrow string
    if Today{
        date_tomorrow = now.Format("02/01/2006")
    }else{
        date_tomorrow = now.AddDate(0, 0, 1).Format("02/01/2006")
    }
    ///////////////////////////////////////////////
    s.start_time = "10:30"
    if len(SlotTime) > 3 {
            s.start_time = SlotTime
    }

    s.facility_id = "eNortjKxUjIysTRTsgZcMBAHAnA~"
    s.date = fmt.Sprintf("%s",date_tomorrow)
    /////////////////////////////////////////////
    fmt.Println("booking for user :" , strings.ToLower(User), "for start time :", s.start_time)

    if strings.ToLower(User) == "isha" {
        s.username = "7899919273"
        s.password = "t6389025"
        s.block = "14773"
        s.ru_id = "206207"
        s.ru_num = "CB - 1201"
    }else if strings.ToLower(User) == "akhil"{
        s.username = "akhilsikri@gmail.com"
        s.password = "EB1404"
        s.block = "24497"
        s.ru_id = "368875"
        s.ru_num = "EB - 1404"
    }else if strings.ToLower(User) == "mehul"{
        s.username = "mehul.sethi2013@vitalum.ac.in"
        s.password = "badmintonlogin123"
        s.block = "7420"
        s.ru_id = "107394"
        s.ru_num = "BA - 0601"
    }else if strings.ToLower(User) == "vishant"{
        s.username = "rahull.krr204@gmail.com"
        s.password = "rahul@403"
        s.block = "7427"
        s.ru_id = "108036"
        s.ru_num = "JC - 0302"
    }else{
        s.username = "7899919273"
        s.password = "t6389025"
        s.block = "14773"
        s.ru_id = "206207"
        s.ru_num = "CB - 1201"
    }
    /////////////////////////////////////////
    if len(params) > 0 {
        s.slot = params[0]
    }else{
        s.slot.id = "2315"
        s.slot.start_hrs = "14"
        s.slot.start_mins = "0"
        s.slot.end_hrs = "14"
        s.slot.end_mins = "30"
    }
    if TestMode{
        fmt.Println(*s)  
    }

    
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////  ISHA settings 
/////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// func (s *Setup) Load(params ...Slot){
//     loc, _ := time.LoadLocation("Asia/Kolkata")
//     now := time.Now().In(loc)
//     date_tomorrow := now.AddDate(0, 0, 1).Format("02/01/2006")
//     ///////////////////////////////////////////////
//     s.start_time = "21:00"
//     /////////////////////////////////////////////
//     s.date = fmt.Sprintf("%s",date_tomorrow)
//     s.username = "7899919273"
//     s.password = "t6389025"
//     s.facility_id = "eNortjKxUjIysTRTsgZcMBAHAnA~"
//     s.block = "14773"
//     s.ru_id = "206207"
//     s.ru_num = "CB - 1201"
//     /////////////////////////////////////////
//     if len(params) > 0 {
//         s.slot = params[0]
//     }else{
//         s.slot.id = "2315"
//         s.slot.start_hrs = "14"
//         s.slot.start_mins = "0"
//         s.slot.end_hrs = "14"
//         s.slot.end_mins = "30"
//     }
    
// }
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////  end ISHA settings 
/////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////  AKHIL settings 
/////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// func (s *Setup) Load(params ...Slot){
//     loc, _ := time.LoadLocation("Asia/Kolkata")
//     now := time.Now().In(loc)
//     date_tomorrow := now.AddDate(0, 0, 1).Format("02/01/2006")
//     ///////////////////////////////////////////////
//     s.start_time = "10:30"
//     /////////////////////////////////////////////
//     s.date = fmt.Sprintf("%s",date_tomorrow)
//     s.username = "akhilsikri@gmail.com"
//     s.password = "EB1404"
//     s.facility_id = "eNortjKxUjIysTRTsgZcMBAHAnA~"
//     s.block = "24497"
//     s.ru_id = "368875"
//     s.ru_num = "EB - 1404"
//     /////////////////////////////////////////
//     if len(params) > 0 {
//         s.slot = params[0]
//     }else{
//         s.slot.id = "2315"
//         s.slot.start_hrs = "14"
//         s.slot.start_mins = "0"
//         s.slot.end_hrs = "14"
//         s.slot.end_mins = "30"
//     }
    
// }
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////  end AKHIL settings 
/////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////  Mehul settings 
/////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// func (s *Setup) Load(params ...Slot){
//     loc, _ := time.LoadLocation("Asia/Kolkata")
//     now := time.Now().In(loc)
//     date_tomorrow := now.AddDate(0, 0, 1).Format("02/01/2006")
//     ///////////////////////////////////////////////
//     s.start_time = "10:30"
//     /////////////////////////////////////////////
//     s.date = fmt.Sprintf("%s",date_tomorrow)
//     s.username = "mehul.sethi@vitalum.ac.in"
//     s.password = "badmintonlogin123"
//     s.facility_id = "eNortjKxUjIysTRTsgZcMBAHAnA~"
//     s.block = "7420"
//     s.ru_id = "107394"
//     s.ru_num = "BA - 0601"
//     /////////////////////////////////////////
//     if len(params) > 0 {
//         s.slot = params[0]
//     }else{
//         s.slot.id = "2315"
//         s.slot.start_hrs = "14"
//         s.slot.start_mins = "0"
//         s.slot.end_hrs = "14"
//         s.slot.end_mins = "30"
//     }
    
// }
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////  end Mehul settings 
/////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////

func MakeBooking(client *http.Client, config Setup) *http.Response{

   defer func() {
        if r := recover(); r != nil {
            fmt.Println("Recovered in f", r)
        }
    }()

    resp, err := client.PostForm("https://www.apnacomplex.com/facilities/member_make_booking", url.Values{
        "facility_id": {config.facility_id},
        "password" : {config.password},
        "is_moderated" : {"0"},
        "booking_type" : {"0"},
        "block" : {config.block},
        "ru_id" : {config.ru_id},
        "booking_category" : {"owners"},
        "description" : {"badminton"},
        "booking_frequency" : {"One Time"},
        "start_booking_date" : {config.date},
        "end_booking_date" : {config.date},
        "booking_date" : {""},
        "facility_time_slot_id" : {config.slot.id},
        "read_instructions" : {"true"},
        "make_booking" : {"Book Now >>"},
        })

    if err != nil {
        fmt.Println()
        log.Fatal(err)
    }
    fmt.Println("trying to confirm booking")
    ConfirmBooking(client, config)
    return resp

}



func ConfirmBooking(client *http.Client, config Setup){
    resp, err := client.PostForm("https://www.apnacomplex.com/facilities/confirm_booking/", url.Values{
        "facility_id": {config.facility_id},
        "is_moderated" : {"0"},
        "is_admin" : {"0"},
        "description" : {"badminton"},
        "fb_category" : {"owners"},
        "booking_category" : {"owners"},
        "facility_time_slot_id" : {config.slot.id},
        "booking_frequency" : {"One Time"},
        "booking_date" : {""},
        "start_booking_date" : {config.date},
        "end_booking_date" : {config.date},
        "booking_start_time_hrs" : {config.slot.start_hrs},
        "booking_start_time_mins" : {config.slot.start_mins},
        "booking_end_time_hrs" : {config.slot.end_hrs},
        "booking_end_time_mins" : {config.slot.end_mins},
        "ru_id" : {config.ru_id},
        "block" : {config.block},
        "ru_num" : {config.ru_num},
        "booking_type" : {"0"},
        "facility_name" : {"Club House - Badminton"},
        "multiple_rates_exist" : {""},
        })

    if err != nil {
        log.Fatal(err)
    }

    doc, err := goquery.NewDocumentFromReader(resp.Body)
    if err != nil {
        log.Fatal(err)
    }


    if TestMode {
        fmt.Println(doc.Html())
    }

    message := doc.Find("#status_message").Text()
    yes := strings.Contains(message, "success")
    if yes {
        fmt.Println("Booking successful")
       
    } else {
        fmt.Println("Error occur " + message)
        fmt.Println("--------------------------------")
    }
    defer resp.Body.Close()
    return 
}



func Login(config Setup) http.Client {
    options := cookiejar.Options{
        PublicSuffixList: publicsuffix.List,
    }
    jar, err := cookiejar.New(&options)
    if err != nil {
        log.Fatal(err)
    }
    client := http.Client{Jar: jar}
    _, err = client.PostForm("https://www.apnacomplex.com/auth/login", url.Values{
    "email": {config.username},
    "password" : {config.password},
    "remember" : {"1"},        
    })
    if err != nil {
        log.Fatal(err)
    }
    return client
}


func main() {
    SetFlags()
    runtime.GOMAXPROCS(2)
    slots := make(map[string]Slot)
    var config Setup
    config.Load()
    //fmt.Println(config)

    
    // use to login into portal
    client := Login(config)


    resp, err := client.Get("https://www.apnacomplex.com/facilities/directory/")
    if err != nil {
        log.Fatal(err)
    }
    doc, err := goquery.NewDocumentFromReader(resp.Body)
    if err != nil {
        log.Fatal(err)
    }
    if TestMode {
        fmt.Println(doc.Html())
    }

    ///////////////////////////////////////////////////////////////////////////////////////////
    /////////// get all time slots and ids START //////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////// 
    resp, err = client.Get("https://www.apnacomplex.com/facilities/member_make_booking/eNortjKxUjIysTRTsgZcMBAHAnA~")
    if err != nil {
        log.Fatal(err)
    }

    doc, err = goquery.NewDocumentFromReader(resp.Body)
    if err != nil {
        log.Fatal(err)
    }


    doc.Find("td select#facility_time_slot_id option").Each(func(i int, s *goquery.Selection) {
        // For each item found, get the band and title
        text := s.Text()
        val , _ := s.Attr("value")
        // fmt.Println(e)
        // fmt.Println("%s >> %s", val,text)
        Make_slots(&slots,val,text)
      })

    // finally load slot for the time we want
    config.Load(slots[config.start_time])
    //////////////////////////////////////////////////////////////////////////////////////////////
    /////////// get all time slots and ids END //////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////////////////////////////////////
    ////// Basic call //////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////
    // use to find slots and make booking
    //////////////////////////////////////////////////
    client = Login(config)
    ///////////////////////////////////////
    ///////////////////////////////////////
   // time.Sleep(40 * time.Second)
    resp = MakeBooking(&client, config)
    if !TestMode {
        for {
            time.Sleep(50 * time.Millisecond)
            go MakeBooking(&client, config)
            go MakeBooking(&client, config)

        }
    }


}
