package main

import (
"fmt"
//"time"
)

func main() {
    defer func() {
        if r := recover(); r != nil {
            fmt.Println("Recovered in msin", r)
        }
    }()

    g(0)
    g(7)
}


func g(i int) {
*
    defer func() {
        if r := recover(); r != nil {
            fmt.Println("Recovered on 3", r)
        }
    }()


    if i == 3 {
        fmt.Println("Panicking!")
        panic(fmt.Sprintf("%v", i))
    }
    fmt.Println("Printing in g = ", i)
    g(i + 1)
}
