package main

import(
"time"
"fmt"

)

func PrepareTime(h,m string)(start,end time.Time){
loc, _ := time.LoadLocation("Asia/Kolkata")
now := time.Now().In(loc)
var hours int
var minutes int
_,_ = fmt.Sscan(h, &hours)
_,_ = fmt.Sscan(m, &minutes)

t := time.Date(now.Year(), now.Month(),now.Day(), hours, minutes, 0, 0, loc)
start = t.Add(time.Second * -20)
end = t.Add(time.Second * 4)

fmt.Println(start)
fmt.Println(end)
return 

}


func main(){
PrepareTime("20","30")
}
