class Singleton:
    instance = None
    val = 0 
    @staticmethod 
    def getInstance():
        if Singleton.instance == None:
            Singleton()
        return Singleton.instance
    def __init__(self):
        if Singleton.instance != None:
            raise Exception("This class is a singleton!")
        else:
            Singleton.instance = self
         
    def inc(self): 
        self.val = self.val + 1
        print self.val
    
    def get(self):
        print self.val
        return self.val 
        
s = Singleton()
print s
s.get()
s.inc()


k = Singleton.getInstance()
print k
k.inc()



d = Singleton.getInstance()
print d
d.get()
d.inc()