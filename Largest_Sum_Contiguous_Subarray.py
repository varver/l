#https://www.geeksforgeeks.org/largest-sum-contiguous-subarray/
# Kadane’s Algorithm

a = [1,2,3,4,5,6,7,8]
def contiguous_sum(arr):
    max_ending_here = 0 
    max_so_far = 0 
    for x in arr : 
        if (max_ending_here + x) > 0 : 
            max_ending_here += x 
        if max_ending_here > max_so_far : 
            max_so_far = max_ending_here 
    print max_so_far


a = [-2, -3, 4, -1, -2, 1, 5, -3]
contiguous_sum(a)



  
