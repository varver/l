def findProfit(price): 
    n = len(price)
    max_price=price[n-1] 
    profit = [0]*n 
    for i in range( n-2, 0 ,-1): 
        if price[i]> max_price: 
            max_price = price[i] 
        profit[i] = max(profit[i+1], max_price - price[i]) 
    min_price=price[0] 
      
    for i in range(1,n): 
        if price[i] < min_price: 
            min_price = price[i] 
        profit[i] = max((price[i]-min_price) + profit[i],profit[i-1]) 
    output = profit[n-1] 
    return output 
  

price = [100, 30, 15, 10, 8, 25, 80]
print "Maximum profit is", findProfit(price) 