class Node: 
    def __init__(self, data): 
        self.data = data 
        self.left  = None
        self.right = None
  
  
def zizagtraversal(root): 
    if root is None: 
        return
  
    current_level = [] 
    next_level = [] 
    left_to_right = True
  
    current_level.append(root) 
  
    while len(current_level) > 0: 
        temp = current_level.pop(-1) 
        print(temp.data) 
  
        if left_to_right: 
            if temp.left: 
                next_level.append(temp.left) 
            if temp.right: 
                next_level.append(temp.right) 
        else: 
            if temp.right: 
                next_level.append(temp.right) 
            if temp.left: 
                next_level.append(temp.left) 
  
        if len(current_level) == 0: 
            left_to_right = not left_to_right 
            current_level, next_level = next_level, current_level 
  
  

root = Node(1) 
root.left = Node(2) 
root.right = Node(3) 
root.left.left = Node(7) 
root.left.right = Node(6) 
root.right.left = Node(5) 
root.right.right = Node(4) 
print("Zigzag Order traversal of binary tree is") 
zizagtraversal(root) 