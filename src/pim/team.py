from __future__ import print_function
import pandas as pd

# If modifying these scopes, delete the file token.pickle.

def main():

    groups = [
        ["Ashish","SE-3","BE","Recon/EWB"],
        ["Annuay","SE-1","BE","Recon"],
        ["Parag","SE-1","BE","Recon"],
        ["Prashant","SE-1","BE","Recon"],
        ["Rishi","SE-1","BE","Recon"],
        ["Sourav","SE-1","BE","Recon"],
        ["Mihir","SE-2","BE","EWB"],
        ["Antony","SE-1","BE","EWB"],
        ["Thanushree","FE-1","FE","EWB"],
        ["Abhinav","FE-1","FE","Recon"],
        ["Harshit","FE-2","FE","New Recon"]
    ]

    data = {"Name": [], "Project" : [],"Role":[],"BE/FE":[]}


    if not data:
        print('No data found.')
    else:
        for x in groups : 
            data["Name"].append(x[0])
            data["Project"].append(x[3])
            data["Role"].append(x[1])
            data["BE/FE"].append(x[2])

    brics = pd.DataFrame(data)
    print("---------------------------------------------------------------------")
    print(brics)
    print("---------------------------------------------------------------------")

if __name__ == '__main__':
    main()
