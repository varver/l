package main

import (
	"fmt"
        "strings"
	"github.com/c-bata/go-prompt"
)


//users := []String{"aman","vimal","jackson","tiago"}

func completer(d prompt.Document) []prompt.Suggest {

	s := []prompt.Suggest{
		{Text: "users", Description: "Store the username and age"},
		{Text: "articles", Description: "Store the article text posted by user"},
		{Text: "exit", Description: "User this command to close this session"},
		{Text: "comments", Description: "Store the text commented to articles"},
	}
//	k := d.Text.strip(" ")
//	fmt.Println(k)
//	fmt.Println("Last word is : "d.Text[d.FindStartOfPreviousWord():])
	return prompt.FilterHasPrefix(s, d.GetWordBeforeCursor(), true)
}

func main() {
   users := []string{"aman","vimal","jackson","tiago"}
   fmt.Println(users)
   for {
	t := prompt.Input("> ", completer)
	fmt.Println("You selected " + t)
	if strings.ToLower(t) == "exit" {break}
}

}
