package main

import "github.com/martinlindhe/notify"
import "github.com/varver/beeep"

func main() {
	// show a notification
	notify.Notify("app name", "notice", "some text", "path/to/icon.png")

	// show a notification and play a alert sound
	notify.Alert("app name", "alert", "some text", "path/to/icon.png")

       err := beeep.Notify("Title", "Message body", "assets/information.png")
if err != nil {
    panic(err)
}



err = beeep.Alert("Title", "Ho Message body", "assets/warning.png")
if err != nil {
    panic(err)
}

}
