import random 
import numpy as np 


class Channel : 
    def __init__(self,name):
        self.name = name 
        self.mean = 0.0
        self.count = 0

    def api_call(self):
        return random.choice([True,False])

    def update_stats(self, success): 
        new_count = self.count + 1 
        value = 0 
        if success : value = 1
        new_mean = (((self.mean * self.count) + value) / new_count )
        self.mean = new_mean

    def trigger(self) : 
        status = self.api_call
        self.update(status)
        return status



class MessageService : 
    def __init__(self,channels,eps):
        self.all_channels = channels 
        self.eps = eps
  

    def add_channel(self,channel):
        self.all_channels.append(channel)


    def explore(self) : 
        return random.choice(self.all_channels)


    def exploit(self): 
        channel = np.argmax([c.mean for c in all_channels])
        return channel 


    def retry(self,channel):
        c = self.exploit()
        if c.name != channel.name : return c 
        return self.retry(channel)


    def send_message(self):
        r = random.random()
        if r < eps : 
            c = self.explore()
        else : 
            c = self.exploit()

        status = c.trigger()
        if not status : 
             self.retry(c)






