import random
    
# assumption that you will get equal distrubtion from ran number generator for values 0-19 as final outcome if we run it 100 times.  
def get_results(): 
    ran = random.randrange(100) % 20
    print("ran= ",ran)
    if ran <=4 :  # 0,1,2,3,4 | total 5 allocation out of 20 = 25% 
        return "win"
    elif ran > 4 and ran <=11 : # 5,6,7,8,9,10,11 | total 7 out of 20 = 35 % 
        return "try again"
    else : # remaning 8 will make 40% of 20
        return "lose"


print(get_results())
