# Hello World program in Python
    
class Node:
    def __init__(self,data):
        self.data = data
        self.left = self.right = None

def insert(root,data):
    if root is None : 
        root = Node(data)
        return root 
    if data < root.data : 
        root.left = insert(root.left, data)
    elif data > root.data : 
        root.right = insert(root.right, data)
    return root 



def level_order(root):
    if root is None:
        return 
    
    current = []
    next = [] 
    current.append(root)
    
    while (len(current) > 0 ):
        temp = current.pop(0)
        print "current node ", temp.data
        if temp.left : next.append(temp.left)
        if temp.right : next.append(temp.right)

        if len(current) == 0 : 
            # print "~~~~~~~~~~~~~~~~~"
            # for i in next : 
            #     print i.data
            # print "~~~~~~~~~~~~~~~~~"
            current , next = next , current



def inorder(root):
    if root is None : 
        return
    inorder(root.left)
    print root.data
    inorder(root.right)


a = [15,10,20,8,12,16,25]
root = None
for x in a : 
    root = insert(root, x)


    
level_order(root)
print "---------------"
print inorder(root)

    
        
    
