
    
# data = [10 , 22 , 9 , 33 , 21 , 50 , 41 , 60]
# def longest(a):
#     n = len(a)
#     max_len = 0 
#     max_till_now = 0 
#     for x in range(n):
#         if x==0 or a[x] <= a[x-1]:
#             max_till_now = 1 
#         elif a[x] > a[x-1]:
#             max_till_now += 1 
#         max_len = max(max_len, max_till_now)
#     print max_till_now
    
# longest(data)


def lis(arr): 
    n = len(arr) 
  
    # Declare the list (array) for LIS and  
    # initialize LIS values for all indexes 
    lis = [1]*n 
  
    # Compute optimized LIS values in bottom up manner 
    for i in range (1 , n): 
        # print ""
        # print "i is = ", i
        for j in range(0 , i): 
            # print "j is = ", j
            # print "arr[i] =" , arr[i] , "| arr[j] = ", arr[j] , " lis[i] = ", lis[i] , " | lis[j] = ", lis[j]  
            # print "----------------------------------"
            if arr[i] > arr[j] and lis[i]< lis[j] + 1 : 
                lis[i] = lis[j]+1
  
    # Initialize maximum to 0 to get  
    # the maximum of all LIS 
    maximum = 0
  
    # Pick maximum of all LIS values 
    for i in range(n): 
        maximum = max(maximum , lis[i]) 
  
    return maximum 
# end of lis function 
  
# Driver program to test above function 
arr = [10, 22, 9, 33, 21, 50, 41, 60] 
print "Length of lis is", lis(arr) 


        
            
            
